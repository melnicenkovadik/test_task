import React from 'react'
import ReactDOM from 'react-dom'
import {App} from './components/App/App'
import ErrorBoundary from './components/ErrorBoundary/ErrorBoundary'
import './index.scss'

const rootElement = document.getElementById('root')
ReactDOM.render(
    <ErrorBoundary>
        <App/>
    </ErrorBoundary>, rootElement)
