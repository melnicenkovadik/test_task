import React, { useEffect, useState } from 'react'
import { Button } from '@material-ui/core'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    margin: 10,
    border: '1px solid black'
  },
  p: {
    color: 'red'
  }
}))

export const Pagination = ({ page, last_page, setPage }) => {
  const classes = useStyles()
  const [pages, setPages] = useState([]);

  useEffect(() => {
    setPages(Array.apply(null, {length: last_page}).map(Number.call, Number))
  }, [page, last_page])

  return (
    <>
      <div>
        {page <= 1
            ? ''
            : <Button onClick={() => setPage(page - 1)}>
              <ArrowBackIosIcon/>
            </Button>}
        {pages.map((item, index) => (
            <span
                key={index}
                style={{
                  cursor: 'pointer',
                  margin: '5px'
                }}
                onClick={() => setPage(index + 1)}>
            <span className={page === index + 1 ? classes.p : ''}>{index + 1}</span>
          </span>
        ))}
        {page >= last_page
            ? ''
            : <Button onClick={() => setPage(page + 1)}>
              <ArrowForwardIosIcon/>
            </Button>}
      </div>
    </>

  )
}
