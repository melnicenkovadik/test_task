import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress'

const useStyles = makeStyles((theme) => ({
  progress: {
    position: 'relative',
    left: '50%',
    top: '50%',
    margin: '0 auto',
    '& > * + *': {
      marginLeft: theme.spacing(2)
    }
  }
}))

export default function CircularIndeterminate () {
  const classes = useStyles()

  return (
    <div className={classes.progress}>
      <CircularProgress color="secondary"/>
    </div>
  )
}
