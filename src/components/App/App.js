import React, {useEffect, useState} from 'react'
import {useFetch} from '../../hooks/useFetch'
import {Comments} from '../Comments'
import {Comment} from '../Comments/Comment'
import {makeStyles} from '@material-ui/core/styles'
import {Button} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    '& > * + *': {
      marginLeft: theme.spacing(2)
    }
  }
}))

export const App = () => {
  const classes = useStyles()
  const [response, setResponse] = useState({
    current_page: 1,
    last_page: 1,
    data: []
  })

  const [{ data, loading }, fetchData] = useFetch({
    instant: false,
    url: 'https://jordan.ashton.fashion/api/goods/30/comments',
  })

  useEffect(() => {
    fetchData({
      url: 'https://jordan.ashton.fashion/api/goods/30/comments?page=1',
      initData: {}
    })
  }, [])

  useEffect(() => {
    if (data) {
      setResponse({
        ...data,
        data: [...response.data, ...data.data]
      })
    }
  }, [data])


  const showMore = () => {
    fetchData({
      url: `https://jordan.ashton.fashion/api/goods/30/comments?page=${data.current_page + 1}`
    })
  }

  const setPage = (page = data.current_page + 1) => {
    setResponse({
      ...response,
      data: []
    })
    fetchData({
      url: `https://jordan.ashton.fashion/api/goods/30/comments?page=${page}`
    })
  }

  return (
    <div className={classes.root}>
      <Comments
          setPage={setPage}
          showMore={showMore}
          last_page={response.last_page}
          page={response.current_page}>
        {response.data?.map((c, index) => {
          return (
              <Comment
                  key={index}
                  name={c.name}
                  data={c.created_at}
                  text={c.text}/>
          )
        }
        )}
        {response.current_page < response.last_page &&
        <Button
            style={{
              backgroundColor: '#84aff6',
            }}
            align="center"
            disabled={loading}
            onClick={showMore}>
          {loading ? 'LOADING...' : 'Show More'}
        </Button>}
      </Comments>
    </div>
  )
}
