import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Pagination } from '../Pagination'
import { AddComment } from './AddComment'

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    margin: 10
  },
  addComment: {
    display: 'flex',
    flexDirection: 'column',
    margin: 10
  },
  comments: {
    margin: 10,
    borderRadius: 10,
    backgroundColor: '#757de8',
    width: '500px'
  }
}))

export const Comments = (props) => {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <div className={classes.comments}>
        {props.children}
        <Pagination {...props} />
      </div>
      <div className={classes.addComment}>
        <AddComment {...props}/>
        <br/>

      </div>
    </div>
  )
}
