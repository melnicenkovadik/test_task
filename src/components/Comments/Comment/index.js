import React from 'react'
import {Avatar, Grid, Paper} from '@material-ui/core'


export const Comment = ({text, name, data}) => {
    return (
        <Paper style={{padding: "10px 10px", margin: "10px"}}>
            <Grid container wrap="nowrap" spacing={2}>
                <Grid item>
                    <Avatar alt={name}>
                        <p>{name}</p>
                    </Avatar>
                </Grid>
                <Grid justifyContent="left" item xs zeroMinWidth>
                    <h4 style={{margin: 0, textAlign: "left"}}>Name</h4>
                    <p style={{textAlign: "left"}}>
                        {text}
                    </p>
                    <p style={{textAlign: "left", color: "gray"}}>
                        <h6>
                            {new Date(data).getDay()[0] < 10 ? new Date(data).getDay() :
                                <span>0{new Date(data).getDay()}</span>}.
                            {new Date(data).getMonth()[0] < 10 ? new Date(data).getMonth() :
                                <span>0{new Date(data).getMonth()}</span>}.
                            {new Date(data).getFullYear()}
                        </h6>
                    </p>
                </Grid>
            </Grid>
        </Paper>
    )
}
