import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Button } from '@material-ui/core'
import { FormInput } from './Input'
import Snackbar from '@material-ui/core/Snackbar'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'

const useStyles = makeStyles(() => ({
  root: {
    backgroundColor: '#3f51b5',
    borderRadius: 10,
    display: 'flex',
    flexDirection: 'column',
    width: '300px',
    margin: 0,
    padding: 10,
    border: '1px solid black'

  },

  btn: {
    backgroundColor: '#84aff6'
  }
}))

export const AddComment = ({ setPage, last_page }) => {
  const [name, setName] = useState('')
  const [text, setText] = useState('')
  const [open, setOpen] = React.useState(false)

  const classes = useStyles()


  const handleSubmit = (event) => {
    event.preventDefault()
    setName('')
    setText('')
    const targetUrl = 'https://jordan.ashton.fashion/api/goods/30/comments'
    fetch(targetUrl, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ name, text })
    })
      .then(function (response) {
        console.table(response)
        setOpen(true)
        setPage(last_page)
        return response.json()
      })
  }

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return
    }
    setOpen(false)
  }

  return (
    <div>
      {
        open
          ? <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left'
          }}
          open={open}
          autoHideDuration={6000}
          onClose={handleClose}
          message="Сповiщення"
          action={
            <React.Fragment>
              <Button color="secondary" size="small" onClick={handleClose}>
                  Коментар успiшно додано
              </Button>
              <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                <CloseIcon fontSize="small"/>
              </IconButton>
            </React.Fragment>
          }
        />
          : null
      }
      <form className={classes.root} onSubmit={handleSubmit}>
        <FormInput
            changeHandler={(event) => setName(event.target.value)}
            name={'name'}
            value={name}
            placeholder={"Ваше iм'я"}/>
        <br/>
        <FormInput
            changeHandler={(event) => setText(event.target.value)}
            name={'text'}
            value={text}
            placeholder={'Ваш коментар...'}/>
        <Button
            disabled={!(name[0] && text[0])}
            className={classes.btn}
            type="submit">
          Надiслати
        </Button>
      </form>
    </div>
  )
}
