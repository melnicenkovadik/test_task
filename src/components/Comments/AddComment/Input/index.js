import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Input } from '@material-ui/core'

const useStyles = makeStyles(() => ({
  clearFix: {
    width: '100%',
    margin: 5
  }
}))

export const FormInput = ({ placeholder, changeHandler, name, value }) => {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <Input
        className={classes.clearFix}
        placeholder={placeholder}
        type="text"
        value={value}
        name={name}
        onChange={changeHandler}
      />
    </div>
  )
}
